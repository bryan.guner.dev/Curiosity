{
  "data": {
    "markdownRemark": {
      "html": "<p>The key to algorithm design and problem solving in general is to constantly ask questions to guide your thought process and to answer those questions with clear explanations.</p>\n<p>You should first have a <em>strategy</em> (a framework around which to build a path to a goal) before thinking about <em>tactics</em> (particular implementation for a solution). For example, a <em>strategy</em> would be the decision to model the problem as a graph algorithm problem whereas the decision to use an adjacency list rather than an adjacency matrix would be a <em>tactical</em> one.</p>\n<h3>List of Questions to Ask:</h3>\n<ol>\n<li>\n<p>Do I really understand the problem?</p>\n</li>\n<li>\n<p>a) What is the input?</p>\n</li>\n<li>\n<p>b) What is the output?</p>\n</li>\n<li>\n<p>c) Can I create an input example small enough to calculate by hand? What happens when I try to solve the problem with that example?</p>\n</li>\n<li>\n<p>d) How important is it to find the optimal answer? Can I settle for something close?</p>\n</li>\n<li>\n<p>e) How large is a typical instance of my problem? How many items will I generally be working with?</p>\n</li>\n<li>\n<p>f) How important is speed? In what timeframe does this algorithm need to run in?</p>\n</li>\n<li>\n<p>g) How much time and effort can I put into this Do I have to create the algorithm in a day or can I take my time and experiment to find the best approach?</p>\n</li>\n<li>\n<p>h) What kind of problem is this? Graph? Numerical? String? Decision?</p>\n</li>\n<li>\n<p>Can I find a simple algorithm or heuristic for my problem?</p>\n</li>\n<li>\n<p>a) Can I use a brute force approach?</p>\n<ul>\n<li>i - If I use a brute force approach, does it give me the right answer?</li>\n<li>ii - How do I measure the quality of the solution?</li>\n<li>iii - What is the Big O time complexity of this solution? Is the problem small enough that that run-time will suffice?</li>\n</ul>\n</li>\n<li>\n<p>b) Can I solve this by repeatedly trying a simple rule (i.e., choosing the smallest number)?</p>\n<ul>\n<li>i - If so, on what type of input does this work well? Does that fit the type of input?</li>\n<li>ii - On what type of input does this work badly? If I can't find any examples, can I show that it will always work well?</li>\n<li>iii - How fast does this heuristic come up with an answer? Does it have a simple implementation?</li>\n</ul>\n</li>\n<li>\n<p>Is my problem in the catalog of algorithms in the back of this book?</p>\n</li>\n<li>\n<p>a) WHat is known about it? Is there an implementation I could use?</p>\n</li>\n<li>\n<p>b) Did I look in the right place for it? Did I look through the pictures and check the index for key words?</p>\n</li>\n<li>\n<p>c) Are there resources relevant to the problem on the internet? Did I search Google/Google Scholar/<a href=\"http://www.algorist.com/algorist.html\">http://www.algorist.com/algorist.html</a> ?</p>\n</li>\n<li>\n<p>Are there special cases of the problem I know how to solve?</p>\n</li>\n<li>\n<p>a) Can I solve this efficiently if I ignore some input parameters?</p>\n</li>\n<li>\n<p>b) Is the problem easier if I set some input parameters to trivial values like 0 or 1?</p>\n</li>\n<li>\n<p>c) Can I simplify the problem? Will that allow me to solve it efficiently?</p>\n</li>\n<li>\n<p>d) Why can't this special case algorithm be generalized to a wider class of inputs?</p>\n</li>\n<li>\n<p>e) Is my problem already a special case of a more generalized problem in the catalog?</p>\n</li>\n<li>\n<p>Which of the standard algorithm design paradigms are most relevant to my problem?</p>\n</li>\n<li>\n<p>a) Is there a set of items that can sorted? Does sorted order make it easier to find a solution?</p>\n</li>\n<li>\n<p>b) Is there a way to split the problem into smaller problems, perhaps using binary search? What about partitioning left from right or big from small? Does this suggest a divide-and-conquer solution?</p>\n</li>\n<li>\n<p>c) Do the input objects or desired solution have a natural left to right order? Can I use dynamic programming to exploit this?</p>\n</li>\n<li>\n<p>d) Are there some operations being done repeatedly? Can I use a data structure to speed this up?</p>\n</li>\n<li>\n<p>e) Can I use random sampling to select which object to pick next? Can I construct random configurations and pick the best one? Can I use directed randomness like simulated annealing to hone in on the best solution?</p>\n</li>\n<li>\n<p>f) Can I structure the problem as linear? Can I turn it into an integer program?</p>\n</li>\n<li>\n<p>g) Does this seem like an NP-Complete problem? Is it possible that there isn't an efficient solution?</p>\n</li>\n<li>\n<p>Am I still stumped?</p>\n</li>\n<li>\n<p>a) Am I willing to hire an expert to tell me what to do?</p>\n</li>\n<li>\n<p>b) Why don't I go back through the questions and work through them again? Did my answers change?</p>\n</li>\n</ol>\n<p>Problem solving is part art and part skill.</p>\n<p>Recommended reading <em>How to Solve It</em> by Polya</p>",
      "frontmatter": {
        "date": "2019-01-28",
        "title": "Algorithm Design Manual:",
        "chapter": "Chapter Ten",
        "subtitle": "How to Design Algorithms",
        "tags": ["algorithms", "algorithm design", "Algorithm Design Manual"],
        "path": "/Algorithm-Design-Manual--Chapter-Ten"
      }
    }
  },
  "pageContext": {
    "isCreatedByStatefulCreatePages": false,
    "pathSlug": "/Algorithm-Design-Manual--Chapter-Ten",
    "prev": {
      "frontmatter": {
        "path": "/Algorithm-Design-Manual--Chapter-Eight",
        "title": "Algorithm Design Manual:",
        "tags": [
          "algorithms",
          "dynamic programming",
          "Algorithm Design Manual"
        ],
        "subtitle": "Dynamic Programming",
        "chapter": "Chapter Eight"
      }
    },
    "next": {
      "frontmatter": {
        "path": "/Computer-Science-Distilled-Chapter-I-Continued",
        "title": "Computer Science Distilled",
        "tags": [
          "computer science",
          "Computer Science Distilled",
          "logic",
          "probability",
          "counting"
        ],
        "subtitle": "1.2 Logic, 1.3 Counting && 1.4 Probability",
        "chapter": "Chapter One"
      }
    }
  }
}
